import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcountEditComponent } from './acount-edit/acount-edit.component';
import { ClientEditComponent } from './client-edit/client-edit.component';
import { MainCuentaComponent } from './main-cuenta/main-cuenta.component';

const routes: Routes = [
  {
    path: 'edit',
    component: ClientEditComponent
  },
  {
    path: 'acount',
    component: MainCuentaComponent
  },
  {
    path: 'acount-edit',
    component: AcountEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuentaRoutingModule {}
