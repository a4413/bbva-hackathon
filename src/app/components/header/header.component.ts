import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  
  public token: boolean = false;

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
  ) { }

  ngOnInit(): void {
    let tokenLocal = localStorage.getItem('token');
    tokenLocal == null ? this.token = false : this.token = true;
  }

  dashboard() {
    this.router.navigate(['dashboard']);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['dashboard']);
  }

  login() {
    this.router.navigate(['login']);
  }

}
