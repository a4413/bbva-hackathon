import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(
    route: ActivatedRouteSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    // let auth = localStorage.getItem('token') != null ? true : false;
    let auth = true;

    if (auth) {
      return true;
    }

    this.router.navigate(['login']);
    return false;
  }
}
