import { Component, OnInit } from '@angular/core';
import {AngularFirestore} from '@angular/fire/firestore';

@Component({
  selector: 'app-external-layout',
  templateUrl: './external-layout.component.html',
  styleUrls: ['./external-layout.component.css']
})
export class ExternalLayoutComponent implements OnInit {

  constructor(private db: AngularFirestore) {
    console.log('prueba');
    const tutorialsRef = db.collection('tutorials');
    const tutorial = { title: 'zkoder Tutorial', url: 'bezkoder.com/zkoder-tutorial' };
    tutorialsRef.add({tutorial}).then((r) =>{
      console.log(r.id);
    }).catch((err) => {
      console.error(err);
    });
  }

  ngOnInit(): void {
  }

}
